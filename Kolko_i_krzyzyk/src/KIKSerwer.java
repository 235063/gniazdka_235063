
import java.awt.BorderLayout;
import java.net.ServerSocket;
import java.net.Socket;
import java.io.IOException;
import java.util.Formatter;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.Condition;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;


public class KIKSerwer extends JFrame
{
    private  int x,y,z;
    private int k;
    private String[] plansza = new String[ 9 ];
    private JTextArea TWiadomosci;
    private Gracz[] gracze;
    private ServerSocket serwer;
    private int aktualnygracz;
    private final static int GraczX = 0;
    private final static int Gracz0 = 1;
    private final static String[] Znaki = { "X", "O" };
    private ExecutorService StartSerwera;
    private Lock blokada;
    private Condition PolaczenieInnegoGracza;
    private Condition TuraInnegoGracza;
    public KIKSerwer()
    {
        super( "Kółko i Krzyżyk - Serwer" );


        StartSerwera = Executors.newFixedThreadPool( 2 );
        blokada = new ReentrantLock();

        PolaczenieInnegoGracza = blokada.newCondition();

        TuraInnegoGracza = blokada.newCondition();

        for ( int i = 0; i < 9; i++ )
            plansza[ i ] = new String( "" );
        gracze = new Gracz[ 2 ];
        aktualnygracz = GraczX;
        try
        {
            serwer = new ServerSocket( 12345, 2 );
        }
        catch ( IOException ioException )
        {
            ioException.printStackTrace();
            System.exit( 1 );
        }

        plansza[0]="a";
        plansza[1]="b";
        plansza[2]="c";
        plansza[3]="d";
        plansza[4]="e";
        plansza[5]="f";
        plansza[6]="g";
        plansza[7]="h";
        plansza[8]="i";
        TWiadomosci = new JTextArea();
        add(TWiadomosci, BorderLayout.CENTER );
        TWiadomosci.setText( "Serwer czeka na połączenia (graczy)\n" );
        setSize( 300, 300 );
        setVisible( true );
    }

    public void wlacz()
    {
        for (int i = 0; i < gracze.length; i++ )
        {
            try
            {
                gracze[ i ] = new Gracz( serwer.accept(), i );
                StartSerwera.execute( gracze[ i ] );
            }
            catch ( IOException ioException )
            {
                ioException.printStackTrace();
                System.exit( 1 );
            }
        }
        blokada.lock();
        try
        {
            gracze[GraczX].UstawBlokade( false );
            PolaczenieInnegoGracza.signal();
        }
        finally
        {
            blokada.unlock();
        }
    }

    private void WyswietlWiadomosc(final String WiadomoscDoWyswietlenia )
    {
        SwingUtilities.invokeLater(
                new Runnable()
                {
                    public void run()             {
                        TWiadomosci.append( WiadomoscDoWyswietlenia );
                    }
                }
        );
    }


    public boolean ZatwierdzenieRuchu(int lokalizacja, int gracz )
    {
        while ( gracz != aktualnygracz)
        {
            blokada.lock(); //blokada dopóki drugi gracz sie nie poruszy

            try
            {
                TuraInnegoGracza.await(); // oczekiwanie na ture
            }
            catch ( InterruptedException exception )
            {
                exception.printStackTrace();
            }
            finally
            {
                blokada.unlock(); // odblokowanie gry z nadejściem tury
            }
        }


        if ( !JestZajety( lokalizacja ) )
        {
            if(KoniecGry())
            {
                return false;
            }
            plansza[ lokalizacja ] = Znaki[aktualnygracz];

            gracze[aktualnygracz].RuchGracza(lokalizacja);

            aktualnygracz = ( aktualnygracz + 1 ) % 2; // zmiana gracza

            // daj znac graczowi, ze nastapil ruch
            gracze[aktualnygracz].RuchDrugiegoGracza( lokalizacja );

            blokada.lock(); // blokada gry

            try
            {
                TuraInnegoGracza.signal(); // zasygnalizuj drugiemu graczowi, ze ma ture
            }
            finally
            {
                if(!KoniecGry())
                    blokada.unlock(); // odblokuj gre, jesli nikt jeszcze nie wygral
                else

                {
                    gracze[aktualnygracz].output.format("Gra skończona gracz %s wygrał\n", plansza[x]);
                    gracze[aktualnygracz].output.flush();
                    blokada.lock();

                }

            }
            return true;
        }
        else
            return false;
    }

    // sprawdzanie, czy kwadrat jest zajety
    public boolean JestZajety(int location )
    {
        if ( plansza[ location ].equals( Znaki[GraczX] ) ||
                plansza[ location ].equals( Znaki[Gracz0] ) )
            return true; // zajety
        else
            return false; // wolny
    }


    public boolean KoniecGry() //sprawdzenie, czy ktorys z graczy wygral
    {

        x=0;y=1;z=2;

        if(plansza[x].equals(plansza[y])&& plansza[x].equals(plansza[z]))
            return true;
        x=0;y=3;z=6;
        if(plansza[x].equals(plansza[y])&& plansza[x].equals(plansza[z]))
            return true;
        x=0;y=4;z=8;
        if(plansza[x].equals(plansza[y])&& plansza[x].equals(plansza[z]))
            return true;
        x=1;y=4;z=7;
        if(plansza[x].equals(plansza[y])&& plansza[x].equals(plansza[z]))
            return true;
        x=2;y=5;z=8;
        if(plansza[x].equals(plansza[y])&& plansza[x].equals(plansza[z]))
            return true;
        x=3;y=4;z=5;
        if(plansza[x].equals(plansza[y])&& plansza[x].equals(plansza[z]))
            return true;
        x=6;y=7;z=8;
        if(plansza[x].equals(plansza[y])&& plansza[x].equals(plansza[z]))
            return true;
        x=2;y=4;z=6;
        if(plansza[x].equals(plansza[y])&& plansza[x].equals(plansza[z]))
            return true;

        return false;
    }


    private class Gracz implements Runnable
    {
        private Socket polaczenie;
        private Scanner input;
        private Formatter output;
        private int NumerGracza;
        private String znak;
        private boolean blokada = true;
        public Gracz(Socket socket, int numer )
        {
            NumerGracza = numer;
            znak = Znaki[NumerGracza];
            polaczenie = socket;
            try
            {
                input = new Scanner( polaczenie.getInputStream() );
                output = new Formatter( polaczenie.getOutputStream() );
            }
            catch ( IOException ioException )
            {
                ioException.printStackTrace();
                System.exit( 1 );
            }
        }
        public void RuchGracza(int lokalizacja)
        {
            output.format("Wykonałeś ruch"+"\n");
            output.format("%d\n",lokalizacja);
            output.flush();
        }
        // wyślij wiadomośc, że drugi gracz się poruszył
        public void RuchDrugiegoGracza(int lokalizacja )
        {
            output.format( "Przeciwnik wykonał ruch\n" );
            output.format( "%d\n", lokalizacja ); // wyślij lokalizacje ruchu
            output.flush();
        }

        public void run()
        {
            try
            {
                WyswietlWiadomosc( "Gracz " + znak + " połączony\n" );
                output.format( "%s\n", znak); // wyślij graczowi jego znak, którym będzie operował
                output.flush();

                if ( NumerGracza == GraczX)
                {
                    output.format( "%s\n%s", "Gracz X połączony",
                            "Oczekiwanie na kolejnego gracza\n" );
                    output.flush();

                    KIKSerwer.this.blokada.lock(); // zablokuj grę w oczekiwaniu na drugiego gracza

                    try
                    {
                        while(blokada)
                        {
                            PolaczenieInnegoGracza.await(); // poczekaj na gracza O
                        }
                    }
                    catch ( InterruptedException exception )
                    {
                        exception.printStackTrace();
                    }
                    finally
                    {
                        KIKSerwer.this.blokada.unlock(); // odblokuj grę po dołączeniu drugiego gracza
                    }
                    output.format( "Drugi gracz połączony, twój ruch\n" );
                    output.flush();
                }
                else
                {
                    output.format( "Gracz O połączony, proszę czekać\n" );
                    output.flush();
                }

                while ( !KoniecGry() )
                {
                    int lokalizacja = 0;

                    if ( input.hasNext() )
                        lokalizacja = input.nextInt(); // pobierz lokalizację ruchu
                    // sprawdź, czy ruch jest możliwy
                    if ( ZatwierdzenieRuchu( lokalizacja, NumerGracza) )
                    {
                        WyswietlWiadomosc( "\nPołożenie: " +lokalizacja );
                        output.format( "Poprawny ruch w %d kratce\n",lokalizacja);
                        output.flush();
                    }
                    else
                    {
                        output.format( "Niepoprawny ruch, spróbuj ponownie\n" );
                        output.flush ();
                    }
                    if(KoniecGry())
                    {
                        output.format("Gra skończona, gracz  %s wygrał\n", plansza[x]);
                        output.flush();
                    }
                }
            }             finally
            {
                try
                {
                    polaczenie.close();
                }
                catch ( IOException ioException )
                {
                    ioException.printStackTrace();
                    System.exit( 1 );
                }             }
        }

        public void UstawBlokade(boolean status )
        {
            blokada = status;
        }
    }
    public static void main( String args[] )
    {
        KIKSerwer gra = new KIKSerwer();
        gra.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
        gra.wlacz();
    }
}