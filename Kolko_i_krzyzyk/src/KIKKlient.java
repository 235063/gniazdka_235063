
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.Socket;
import java.net.InetAddress;
import java.io.IOException;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import java.util.Formatter;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;


public class KIKKlient extends JFrame implements Runnable
{
    private JTextField TGracz;
    private JTextArea TPole;
    private JPanel JPanelPlanszy;
    private JPanel JPanel2;
    private Kwadrat plansza[][];
    private Kwadrat aktualnykwadrat;
    private Socket polaczenie;
    private Scanner input;
    private Formatter output;
    private String host;
    private String znak;
    private boolean tura;
    private final String krzyzyk = "X";
    private final String kolko = "O";


    public KIKKlient(String host )
    {
        this.host = host;
        TPole = new JTextArea( 4, 30 );
        TPole.setEditable( false );
        add( new JScrollPane(TPole), BorderLayout.SOUTH );
        JPanelPlanszy = new JPanel();
        JPanelPlanszy.setLayout( new GridLayout( 3, 3, 0, 0 ) );

        plansza = new Kwadrat[ 3 ][ 3 ];


        for (int wiersz = 0; wiersz < plansza.length; wiersz++ )
        {
            for (int kolumna = 0; kolumna < plansza[ wiersz ].length; kolumna++ )
            {

                plansza[ wiersz ][ kolumna ] = new Kwadrat(  " ",wiersz * 3 + kolumna);
                JPanelPlanszy.add( plansza[ wiersz ][ kolumna ] );
            }
        }

        TGracz = new JTextField();
        TGracz.setEditable( false );
        add(TGracz, BorderLayout.NORTH );

        JPanel2 = new JPanel();
        JPanel2.add(JPanelPlanszy, BorderLayout.CENTER );
        add(JPanel2, BorderLayout.CENTER );

        setSize( 300, 225 );
        setVisible( true );

        WlaczKlienta();
    }


    public void WlaczKlienta()
    {
        try
        {

            polaczenie = new Socket(InetAddress.getLocalHost( ),12345);


            input = new Scanner( polaczenie.getInputStream() );
            output = new Formatter( polaczenie.getOutputStream() );
        }
        catch ( IOException ioException )
        {
            ioException.printStackTrace();
        }


        ExecutorService worker = Executors.newFixedThreadPool( 1 );
        worker.execute( this );
    }


    public void run()
    {
        znak = input.nextLine();

        SwingUtilities.invokeLater(
                new Runnable()
                {
                    public void run()
                    {

                        TGracz.setText( "Jesteś graczem \"" + znak + "\"" );
                    }
                }
        );

        tura = ( znak.equals(krzyzyk) );
        while ( true )
        {
            if ( input.hasNextLine() )
                WyswietlWiadomosc( input.nextLine() );
        }
    }


    private void WyswietlWiadomosc(String wiadomosc )
    {

        if(wiadomosc.equals("Gra skończona gracz %s wygrał"))
        {
            displayMessage(wiadomosc+"\n" );
        }

        else if ( wiadomosc.equals( "Wykonałeś ruch" ) )
        {
            displayMessage( wiadomosc+"\n" );
            int i=input.nextInt();
            input.nextLine();
            WstawZnak( plansza[i/3][i%3], znak); // wstawia znak do kwadratu
        }
        else if(wiadomosc.equals("Poprawny ruch "))
            displayMessage(wiadomosc);

        else if ( wiadomosc.equals( "Niepoprawny ruch, spróbuj ponownie" ) )
        {
            displayMessage( wiadomosc + "\n" );
            tura = true;
        }
        else if ( wiadomosc.equals( "Przeciwnik wykonał ruch" ) )
        {
            int lokalizacja = input.nextInt();
            input.nextLine();
            int row = lokalizacja / 3;
            int column = lokalizacja % 3;
            WstawZnak( plansza[ row ][ column ],
                    ( znak.equals(krzyzyk) ? kolko : krzyzyk) );
            displayMessage( "Przeciwnik wykonał ruch, twoja kolej\n" );
            tura = true;
        }
        else
            displayMessage( wiadomosc + "\n" );
    }


    private void displayMessage( final String WiadomoscDoWyswietlenia )
    {
        SwingUtilities.invokeLater(
                new Runnable()
                {
                    public void run()
                    {
                        TPole.append( WiadomoscDoWyswietlenia );
                    }
                }
        );
    }

    private void WstawZnak(final Kwadrat kwadrat, final String znak )
    {
        SwingUtilities.invokeLater(
                new Runnable()
                {
                    public void run()
                    {
                        kwadrat.ustawznak( znak );
                    }
                }
        );
    }


    public void kliknietykwadrat(int lokalizacja )
    {

        if (tura)
        {
            output.format( "%d\n", lokalizacja );
            output.flush();
            tura = false;
        }
    }




    private class Kwadrat extends JPanel
    {
        private String znak;
        private int lokalizacja;

        public Kwadrat(String oznaczenie, int lokalizacja )
        {
            znak = oznaczenie;
            this.lokalizacja = lokalizacja;

            addMouseListener(
                    new MouseAdapter()
                    {
                        public void mouseReleased( MouseEvent e )
                        {
                            aktualnykwadrat = Kwadrat.this;

                            kliknietykwadrat( lokalizacjakwadratu() );
                        }
                    }
            );
        }


        public Dimension getPreferredSize()
        {
            return new Dimension( 30, 30 );
        }


        public Dimension getMinimumSize()
        {
            return getPreferredSize();
        }

        public void ustawznak(String nowyznak )
        {
            znak = nowyznak;
            repaint();
        }


        public int lokalizacjakwadratu()
        {
            return lokalizacja;
        }

        public void paintComponent( Graphics g )
        {
            super.paintComponent( g );

            g.drawRect( 0, 0, 29, 29 );
            g.drawString(znak, 11, 20 );
        }
    }
    public static void main( String args[] )
    {
        KIKKlient gra;


        if ( args.length == 0 )
            gra = new KIKKlient( "127.0.0.1" );
        else
            gra = new KIKKlient( args[ 0 ] );

        gra.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
    }
}
